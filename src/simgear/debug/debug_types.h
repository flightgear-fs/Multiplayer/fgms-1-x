/** \file debug_types.h
 *  Define the various logging classes and prioritiess
 */

/** 
 * Define the possible classes/categories of logging messages
 */
typedef enum {
    SG_NONE	= 0x00000000,
    SG_IO	= 0x00000001,
    SG_EVENT	= 0x00000002,
    SG_NETWORK	= 0x00000004,
    SG_SYSTEMS	= 0x00000008,
    SG_GENERAL	= 0x00000010,
    SG_ALL	= 0xFFFFFFFF
} sgDebugClass;


/**
 * Define the possible logging priorities (and their order).
 */
typedef enum {
    SG_BULK,	// Less frequent debug type messages
    SG_DEBUG,	// Less frequent debug type messages
    SG_INFO,	// Informatory messages
    SG_WARN,	// Possible impending problem
    SG_ALERT	// Very possible impending problem
} sgDebugPriority;

