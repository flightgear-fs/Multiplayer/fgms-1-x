
#ifndef _STDINT_HXX
#define _STDINT_HXX 1

// Copyright (C) 1999  Curtis L. Olson - http://www.flightgear.org/~curt
//
// Written by Curtis Olson - http://www.flightgear.org/~curt
// Started September 2001.
//
// This file is in the Public Domain, and comes with no warranty.
//
// $Id: stdint.hxx,v 1.1.1.1 2009/10/12 07:24:05 oliver Exp $


//////////////////////////////////////////////////////////////////////
//
//  There are many sick systems out there:
//
//  check for sizeof(float) and sizeof(double)
//  if sizeof(float) != 4 this code must be patched
//  if sizeof(double) != 8 this code must be patched
//
//  Those are comments I fetched out of glibc source:
//  - s390 is big-endian
//  - Sparc is big-endian, but v9 supports endian conversion
//    on loads/stores and GCC supports such a mode.  Be prepared.
//  - The MIPS architecture has selectable endianness.
//  - x86_64 is little-endian.
//  - CRIS is little-endian.
//  - m68k is big-endian.
//  - Alpha is little-endian.
//  - PowerPC can be little or big endian.
//  - SH is bi-endian but with a big-endian FPU.
//  - hppa1.1 big-endian.
//  - ARM is (usually) little-endian but with a big-endian FPU.
//
//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////
//
//  include stdint.h if available,
//  else define stdint types
//
//////////////////////////////////////////////////
#ifdef HAVE_STDINT_H
#   include <stdint.h>
#elif defined( _MSC_VER ) || defined(__MINGW32__) || defined(sun)
typedef signed char        int8_t;
typedef signed short       int16_t;
typedef signed int         int32_t;
typedef __int64            int64_t;
typedef unsigned char      uint8_t;
typedef unsigned short     uint16_t;
typedef unsigned int       uint32_t;
typedef unsigned __int64   uint64_t;
typedef int ssize_t;
#elif defined(sgi) || defined(__sun)
# include <sys/types.h>
#else
# include <stdint.h>
#endif

//////////////////////////////////////////////////
//
//  define __BYTE_ORDER
//  define __LITTELE_ENDIAN
//  define __BIG_ENDIAN
//
//////////////////////////////////////////////////
#ifdef __linux__
#   include <endian.h>
#else
#   define __LITTLE_ENDIAN 1234
#   define __BIG_ENDIAN    4321
#endif

#if defined(__alpha) || defined(__alpha__)
#   ifndef __BYTE_ORDER
#       define __BYTE_ORDER __LITTLE_ENDIAN
#   endif

#elif defined __sgi
#   ifndef __BYTE_ORDER
#       define __BYTE_ORDER __BIG_ENDIAN
#   endif

#elif defined(__CYGWIN__) || defined(_WIN32)
#   ifndef __BYTE_ORDER
#       define __BYTE_ORDER __LITTLE_ENDIAN
#   endif

#endif
#if ((__BYTE_ORDER != __LITTLE_ENDIAN) && (__BYTE_ORDER != __BIG_ENDIAN))
#   error "Architecture not supported."
#   error "please define __BYTE_ORDER for this system"
#endif

#if (__BYTE_ORDER == __LITTLE_ENDIAN)
#   define LOW  1
#   define HIGH 0
#else
#   define SWAP32(arg) arg
#   define SWAP64(arg) arg
#   define LOW  0
#   define HIGH 1
#endif

// Use native swap functions if available, they are much
// faster.
// Don't use byteswap.h on Alpha machines because its buggy
#if defined(__linux__) && !(defined(__alpha) || defined(__alpha__))
#   include <byteswap.h>
#   define SWAP16(arg) bswap_16(arg)
#   define SWAP32(arg) bswap_32(arg)
#   define SWAP64(arg) bswap_64(arg)
#else
    inline uint16_t sg_bswap_16(uint16_t x) {
        x = (x >> 8) | (x << 8);
        return x;
    }

    inline uint32_t sg_bswap_32(uint32_t x) {
        x = ((x >>  8) & 0x00FF00FFL) | ((x <<  8) & 0xFF00FF00L);
        x = (x >> 16) | (x << 16);
        return x;
    }

    inline uint64_t sg_bswap_64(uint64_t x) {
        x = ((x >>  8) & 0x00FF00FF00FF00FFLL)
          | ((x <<  8) & 0xFF00FF00FF00FF00LL);
        x = ((x >> 16) & 0x0000FFFF0000FFFFLL)
          | ((x << 16) & 0xFFFF0000FFFF0000LL);
        x =  (x >> 32) | (x << 32);
        return x;
    }
#   define SWAP16(arg) sg_bswap_16(arg)
#   define SWAP32(arg) sg_bswap_32(arg)
#   define SWAP64(arg) sg_bswap_64(arg)
#endif

#endif // !_STDINT_HXX

