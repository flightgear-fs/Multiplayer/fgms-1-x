#include <iostream>
#include <string.h>
#include <flightgear/Network/netpacket.hxx>
#include <flightgear/Network/netpacket.hxx>
#include <flightgear/Network/md5.hxx>
#include <simgear/debug/debug.hxx>

using namespace std;

const int BUFSIZE = 200;

int TestBuffer ( NetPacket::eBUFFER_ENCODING_TYPE type )
{
	NetPacket Buf ( BUFSIZE );
	char*     Str = ( char* ) "the quick brown fox jumps over the fence";
	int       NumErrors = 0;
	int       NumTest = 1;
	SG_LOG ( SG_IO, SG_DEBUG, "testing encoding type " << type );
	Buf.SetEncoding ( type );
	try
	{
		//////////////////////////////////////////////////
		//  write some data to the buffer
		//////////////////////////////////////////////////
		uint32_t len = strlen ( Str );
		Buf.Skip ( 2 );
		NumTest++;
		Buf.Write_int8 ( 1 );
		NumTest++;
		Buf.Write_int8 ( 2 );
		NumTest++;
		Buf.Write_int8 ( 3 );
		NumTest++;
		Buf.Write_int8 ( 4 );
		NumTest++;
		Buf.Write_int16 ( -4660 );
		NumTest++;
		Buf.Write_uint16 ( 4660 );
		NumTest++;
		Buf.Write_int32 ( 0x123456 );
		NumTest++;
		Buf.Write_int64 ( 0x1234567890aabbccULL );
		NumTest++;
		Buf.Write_float ( 0.123f );
		NumTest++;
		Buf.Write_double ( 0.123456 );
		NumTest++;
		Buf.WriteOpaque ( Str, len+1 );
		NumTest++;
		Buf.Write_int8 ( 1 );
		NumTest++;
		Buf.Write_uint8 ( 255 );
		NumTest++;
		Buf.Write_int8 ( 2 );
		NumTest++;
		Buf.Write_int8 ( 127 );
		NumTest++;
		Buf.Write_uint8 ( 128 );
		NumTest++;
		Buf.Write_uint16 ( 0xffff );
		NumTest++;
		//////////////////////////////////////////////////
		//  read the data from the buffer and
		//  check if it is correct
		//////////////////////////////////////////////////
		int64_t  Data;
		double       Floating;
		char         ReadStr[255];
		NumTest = 1;
		Buf.Start ();
		Buf.SetIndex ( 2 );
		Data = Buf.Read_int8 ();
		NumTest++;
		if ( Data != 1 )
		{
			cout << "read should be 1 but is " << Data << endl;
			NumErrors++;
		}
		Data = Buf.Read_int8 ();
		NumTest++;
		if ( Data != 2 )
		{
			cout << "read should be 2 but is " << Data << endl;
			NumErrors++;
		}
		Data = Buf.Read_int8 ();
		NumTest++;
		if ( Data != 3 )
		{
			cout << "read should be 3 but is " << Data << endl;
			NumErrors++;
		}
		Data = Buf.Read_int8 ();
		NumTest++;
		if ( Data != 4 )
		{
			cout << "read should be 4 but is " << Data << endl;
			NumErrors++;
		}
		Data = Buf.Read_int16 ();
		NumTest++;
		if ( Data != -4660 )
		{
			cout << "read should be -4660 but is " << Data << endl;
			NumErrors++;
		}
		Data = Buf.Read_uint16 ();
		NumTest++;
		if ( Data != 4660 )
		{
			cout << "read should be 4660 but is " << Data << endl;
			NumErrors++;
		}
		Data = Buf.Read_int32 ();
		NumTest++;
		if ( Data != 1193046 )
		{
			cout << "read should be 123456 but is " << Data << endl;
			NumErrors++;
		}
		Data = Buf.Read_int64 ();
		NumTest++;
		if ( Data != 1311768467294829516ULL )
		{
			cout << "read should be 1311768467294829516 but is "
			     << Data << endl;
			NumErrors++;
		}
		Floating = Buf.Read_float ();
		NumTest++;
		if ( Floating != ( float ) 0.123 )
		{
			cout << "read should be 0.123 but is " << Floating << endl;
			NumErrors++;
		}
		Floating = Buf.Read_double ();
		NumTest++;
		if ( Floating != 0.123456 )
		{
			cout << "read should be 0.123456 but is " << Floating << endl;
			NumErrors++;
		}
		Buf.ReadOpaque ( ReadStr, len );
		NumTest++;
		if ( strncmp ( Str, ReadStr, len ) )
		{
			cout << "read str should be " << endl
			     << " '" << Str << "'" << endl
			     << " but is '" << ReadStr << "'" << endl;
			NumErrors++;
		}
		Data = Buf.Read_uint8 ();
		NumTest++;
		if ( Data != 1 )
		{
			cout << "read should be 1 but is " << Data << endl;
			NumErrors++;
		}
		Data = Buf.Read_uint8 ();
		NumTest++;
		if ( Data != 255 )
		{
			cout << "read should be 255 but is " << Data << endl;
			NumErrors++;
		}
		Data = Buf.Read_uint8 ();
		NumTest++;
		if ( Data != 2 )
		{
			cout << "read should be 2 but is " << Data << endl;
			NumErrors++;
		}
		Data = Buf.Read_int8 ();
		NumTest++;
		if ( Data != 127 )
		{
			cout << "read should be 127 but is " << Data << endl;
			NumErrors++;
		}
		Data = Buf.Read_uint8 ();
		NumTest++;
		if ( Data != 128 )
		{
			cout << "read should be 128 but is " << Data << endl;
			NumErrors++;
		}
		Data = Buf.Read_uint16 ();
		NumTest++;
		if ( Data != 65535 )
		{
			cout << "read should be 65535 but is " << Data << endl;
			NumErrors++;
		}
	}
	catch ( sg_exception ex )
	{
		cout << "got exception in test #" << NumTest << endl;
		cout << ex.getFormattedMessage () << endl;
	}
	if ( NumErrors )
	{
		cout << "###" << endl;
		cout << "### " << NumErrors << " errors found!" << endl;
		cout << "###" << endl;
		cout << "### Buffer is:" << endl;
		sg_debug::Dump ((void*)Buf.Buffer(), BUFSIZE);
		return ( NumErrors );
	}
	return ( 0 );
}

int TestBufferCrypt ( string Password )
{
	int NumErrors = 0;
	NetPacket Buf ( BUFSIZE );
	MD5 md5 = MD5 ( Password );
	uint32_t* Key = ( uint32_t* ) md5.get_digest ();
	SG_LOG ( SG_IO, SG_DEBUG, "testing buffer encryption " );
	Buf.WriteString ( "abcdefghijklmnopqrstuvwxyz" );
	Buf.WriteString ( "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
	Buf.WriteString ( "0123456789" );
	// sg_debug::Dump ((void*) Buf.Buffer(), BUFSIZE);
	Buf.Encrypt ( Key );
	// sg_debug::Dump ((void*) Buf.Buffer(), BUFSIZE);
	Buf.Decrypt ( Key );
	// sg_debug::Dump ((void*) Buf.Buffer(), BUFSIZE);
	Buf.Start ();
	string Data;
	Data = Buf.ReadString();
	if ( Data != "abcdefghijklmnopqrstuvwxyz" )
	{
		cout << "read should be 'abcdefghijklmnopqrstuvwxyz' but is " << Data << endl;
		NumErrors++;
	}
	Data = Buf.ReadString();
	if ( Data != "ABCDEFGHIJKLMNOPQRSTUVWXYZ" )
	{
		cout << "read should be 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' but is " << Data << endl;
		NumErrors++;
	}
	Data = Buf.ReadString();
	if ( Data != "0123456789" )
	{
		cout << "read should be '0123456789' but is " << Data << endl;
		NumErrors++;
	}
	return ( NumErrors );
}

int main ()
{
	int Errors = 0;
//sglog().setLogLevels( SG_IO, SG_DEBUG );     // NetPacket
	sglog().setLogLevels ( SG_IO, SG_BULK );     // tiny_xdr
	Errors += TestBuffer ( NetPacket::XDR );
	Errors += TestBuffer ( NetPacket::NET );
	Errors += TestBuffer ( NetPacket::NONE );
	Errors += TestBufferCrypt ( "blafasel12" );
	if ( Errors == 0 )
	{
		cout << "OK" << endl;
		SG_LOG ( SG_IO, SG_DEBUG, "###" );
		SG_LOG ( SG_IO, SG_DEBUG, "### All tests are OK" );
		SG_LOG ( SG_IO, SG_DEBUG, "###" );
	}
	return ( Errors );
}
