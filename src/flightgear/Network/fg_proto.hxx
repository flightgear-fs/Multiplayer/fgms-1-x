/// @file fg_proto.hxx
/// A class for handling the multiplayer protocol
///
/// @author	Oliver Schroeder <fgms@o-schroeder.de>
/// @date	2006-2015
/// @copyright	GPLv3
///

// Copyright (C) Oliver Schroeder <fgms@postrobot.de>
//
// This file is part of fgms, the flightgear multiplayer server
//
// fgms is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// fgms is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with fgms.  If not, see <http://www.gnu.org/licenses/>
//

#ifndef FG_PROTO_HEADER
#define FG_PROTO_HEADER

#include <map>
#include <simgear/props/props.hxx>
#include <simgear/props/props_io.hxx>
#include <simgear/structure/exception.hxx>
#include <simgear/misc/stdint.hxx>
#include <simgear/debug/debug.hxx>
#include <simgear/debug/logstream.hxx>
#include <flightgear/Network/udp_peer.hxx>

namespace fgms
{

typedef std::map<uint32_t, string>	t_ID2Properties;
typedef std::map<string, uint32_t>	t_Properties2ID;
typedef	std::list<uint32_t>		t_PropertyList;

/**
 * These must be handled in m_WriteSpecial and m_ReadSpecial!
 */
typedef enum
{
	REGISTER_SERVER,///< server requests to be part of our network
	REGISTER_FGFS,	///< fgfs requests to be part of our network
	SERVER_QUIT,	///< tell clients that I (a server) quit the network
	FGFS_QUIT,	///< tell clients that I (a fgfs) quit the network
	REGISTER_OK,	///< tell server that it is registered
	GENERAL_ERROR,	///< inform client of an error
	SET_CLIENT_ID,	///< tell client its ID
	AUTH_CLIENT,	///< fgms sends auth-request to fgas
	AUTH_OK,	///< fgas tells fgms authentication is OK
	AUTH_FAILED,	///< tell client about authentication failure
	AUTHENTICATED,	///< fgms tells fgfs authentication is OK
	PING,		///< ping a client
	PONG,		///< client responds with pong
	USE_FGAS,	///< tell fgms which fgas to use
	NO_FGAS,	///< tell fgms that there is no fgas currently online
	YOU_ARE_HUB,	///< tell fgms that is the HUB server
	YOU_ARE_LEAVE,	///< tell fgms that is a LEAVE server
	NEW_SERVER,	///< tell fgls about a new server
	REQUEST_SERVER,	///< fgfs searching for a server
	USE_SERVER,	///< tell fgfs which server to use
	REGISTER_AGAIN,	///< tell a client to register again
	INTERNAL_COMMAND ///< mark the end of the list. Used to check if it is a command
} Commands;

}; // namespace fgms

class ProtocolHandler
{
public:
	ProtocolHandler ();
	/// Read a config.xml into Properties
	fgms::eRETURNVALS ReadConfig ( const string& FileName,
	  SGPropertyNode& m_Properties );
	/// Read the protocol specification
	fgms::eRETURNVALS ReadProtoSpec ( const string& Filename,
	  const SGPropertyNode& m_Properties );
	/// Read Properties vom packet and store them in Properties
	fgms::eRETURNVALS ParsePacket ( t_ReceivePacket* Msg, UDP_Peer& Peer, 
	  fgms::t_PropertyList& ReceivedProperties,
	  SGPropertyNode &Properties ) throw ( sg_exception );
	/// Write Props to Peer
	fgms::eRETURNVALS WriteProps ( UDP_Peer& Peer,
	  const SGPropertyNode& m_Properties,
	  fgms::t_PropertyList& SendProps,
	  const UDP_Peer::eNETPACKET_FLAGS Flags = UDP_Peer::UNRELIABLE);
	void SetSenderID ();
protected:
	UDP_Peer		m_Peer;
	bool			m_SetSenderID;
	SGPropertyNode		m_Properties;
	fgms::t_ID2Properties	m_ID2Prop;
	fgms::t_Properties2ID	m_Prop2ID;
	fgms::t_PropertyList	m_ToSendProperties;
	fgms::t_PropertyList	m_OneTimeProperties;
	fgms::t_PropertyList	m_OnChangeProperties;
	// Skip over an unknown property
	fgms::eRETURNVALS m_Skip ( uint32_t Type, NetPacket* Msg );
	void	m_ReadSpecial  ( uint32_t Code, UDP_Peer& Peer,
	  NetPacket* Msg, fgms::t_PropertyList& ReceivedProperties );
	void	m_WriteSpecial ( uint32_t Code, UDP_Peer& Peer,
	  UDP_Peer::eNETPACKET_FLAGS Flags );
}; // class ProrocolHandler

#endif
