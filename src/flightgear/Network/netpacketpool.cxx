// netpacketpool.hxx -  NetPacket is a buffer for network packets
//
// This file is part of fgms
//
// Copyright (C) 2006-2010  Oliver Schroeder
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of
// the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see
// <http://www.gnu.org/licenses/>.
//


#include "netpacketpool.hxx"
#include <simgear/debug/debug.hxx>
#include <simgear/debug/logstream.hxx>

//////////////////////////////////////////////////////////////////////
NetPacketPool::NetPacketPool
()
{
    SG_TRACE_START
    m_Capacity    = 1400;
    m_MinBuffers  = 3;
    m_MaxBuffers  = 10;
    m_ClearBuffers  = false;
    Maintain ();
    SG_TRACE_END
} // NetPacketPool::NetPacketPool ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
NetPacketPool::~NetPacketPool
()
{
    SG_TRACE_START
    if (m_Buffers.size ())
    {
        std::list<NetPacket*>::iterator Current;
        NetPacket* Buffer;
        Current = m_Buffers.begin ();
        while (Current != m_Buffers.end ())
        {
            Buffer = (*Current);
            delete (Buffer);
            m_Buffers.erase (Current);
            Current = m_Buffers.begin ();
        }
    }
    SG_TRACE_END
} // NetPacketPool::NetPacketPool ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
NetPacketPool::NetPacketPool
(
    const uint32_t Capacity,
    const int MaxBuffers,
    const int MinBuffers
)
{
    SG_TRACE_START
    m_Capacity    = Capacity;
    m_MinBuffers  = MinBuffers;
    m_MaxBuffers  = MaxBuffers;
    m_ClearBuffers  = false;
    m_AddBuffers (m_MaxBuffers);
    SG_TRACE_END
} // NetPacketPool::NetPacketPool ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
void
NetPacketPool::SetCapacity
(
    const uint32_t Capacity
)
{
    SG_TRACE_START
    m_Capacity = Capacity;
    Maintain ();
    SG_TRACE_END
} // NetPacketPool::SetCapacity ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
void
NetPacketPool::SetMinBuffers
(
    const int MinBuffers
)
{
    SG_TRACE_START
    m_MinBuffers = MinBuffers;
    Maintain ();
    SG_TRACE_END
} // NetPacketPool::SetMinBuffers ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
void
NetPacketPool::SetMaxBuffers
(
    const int MaxBuffers
)
{
    SG_TRACE_START
    m_MaxBuffers = MaxBuffers;
    Maintain ();
    SG_TRACE_END
} // NetPacketPool::SetMaxBuffers ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
void
NetPacketPool::SetClearBuffers
(
    const bool ClearBuffers
)
{
    SG_TRACE_START
    m_ClearBuffers = ClearBuffers;
    SG_TRACE_END
} // NetPacketPool::SetClearBuffers ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
NetPacket* NetPacketPool::GetBuffer
()
{
    SG_TRACE_START
    NetPacket* Buffer = 0;
    //////////////////////////////////////////////////
    //  search for a free buffer of requested
    //  capacity in the pool
    //////////////////////////////////////////////////
    while ((m_Buffers.size()) && (Buffer == 0))
    {
        Buffer = m_Buffers.back ();
        m_Buffers.pop_back ();
        if (Buffer)
        {
            if (Buffer->Capacity () == m_Capacity)
            {
                SG_DEBUG_OUT ("- buffer found");
                SG_TRACE_END
                return (Buffer);
            }
            delete Buffer;
            Buffer = 0;
        }
    }
    //////////////////////////////////////////////////
    //  there is no such buffer in the pool,
    //  so create a new one
    //////////////////////////////////////////////////
    SG_DEBUG_OUT ("- creating new buffer");
    Buffer = new NetPacket (m_Capacity);
    if (m_ClearBuffers)
    {
        Buffer->Clear ();
    }
    SG_TRACE_END
    return (Buffer);
} // NetPacketPool::GetBuffer ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
void
NetPacketPool::RecycleBuffer
(
    NetPacket* & Buffer
)
{
    SG_TRACE_START
    //////////////////////////////////////////////////
    //  push the returned buffer into the pool
    //  if it is of the current capacity of our
    //  pool.
    //////////////////////////////////////////////////
    if (Buffer->Capacity() == m_Capacity)
    {
        if (m_ClearBuffers)
        {
            Buffer->Clear ();
        }
        m_Buffers.push_back (Buffer);
        Buffer = 0;
        SG_DEBUG_OUT ("- cleared and recycled");
        SG_TRACE_END
        return;
    }
    //////////////////////////////////////////////////
    //  It is not of the desired capacity, so
    //  simply free and drop it.
    //////////////////////////////////////////////////
    SG_DEBUG_OUT ("- buffer deleted");
    delete Buffer;
    Buffer = 0;
    SG_TRACE_END
} // NetPacketPool::RecycleBuffer ( NetPacket* Buffer )
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
void
NetPacketPool::m_AddBuffers
(
    const int NumBuffers
)
{
    SG_TRACE_START
    NetPacket* Buffer;

    for (int i = 0; i < NumBuffers; i++)
    {
        Buffer = new NetPacket (m_Capacity);
        if (m_ClearBuffers)
        {
            Buffer->Clear ();
        }
        m_Buffers.push_back (Buffer);
    }
    SG_TRACE_END
} // NetPacketPool::m_AddBuffers ()
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
void
NetPacketPool::Maintain
()
{
    SG_TRACE_START
    //////////////////////////////////////////////////
    //  Walk through the pool and check if all
    //  buffers have the desired capacity.
    //////////////////////////////////////////////////
    if (m_Buffers.size ())
    {
        std::list<NetPacket*>::iterator Current;
        Current = m_Buffers.begin ();
        while (Current != m_Buffers.end ())
        {
            if ((*Current)->Capacity() != m_Capacity)
            {
                SG_DEBUG_OUT ("- buffer with wrong capacity: deleted");
                std::list<NetPacket*>::iterator Tmp;
                Tmp = Current;
                delete (*Tmp);
                Current = m_Buffers.erase (Tmp);
            }
            else
            {
                Current++;
            }
        }
    }
    //////////////////////////////////////////////////
    //  If we have less the m_MinBuffers buffers
    //  left in the pool, create buffers until
    //  we have the minimum number of buffers
    //  again.
    //////////////////////////////////////////////////
    if (m_Buffers.size () < m_MinBuffers)
    {
        SG_DEBUG_OUT ("- insufficient buffers, adding");
        m_AddBuffers (m_MinBuffers - m_Buffers.size ());
    }
    //////////////////////////////////////////////////
    //  If we have more than m_MaxBuffers buffers
    //  in the pool, delete the overplus.
    //////////////////////////////////////////////////
    NetPacket* Buffer;
    while (m_Buffers.size () > m_MaxBuffers)
    {
        SG_DEBUG_OUT ("- too many buffers, deleting");
        Buffer = m_Buffers.back ();
        delete (Buffer);
        m_Buffers.pop_back();
    }
    SG_TRACE_END
} // NetPacketPool::Maintain ()
//////////////////////////////////////////////////////////////////////

// vim: ts=4:sw=4:sts=0

