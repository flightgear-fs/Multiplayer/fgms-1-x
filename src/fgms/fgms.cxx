/// @file fgms.cxx
/// multiplayer server for FlightGear
///
/// @author	Oliver Schroeder <fgms@o-schroeder.de>
/// @date	2005-2015
/// @copyright	GPLv3
///

// Copyright (C) Oliver Schroeder <fgms@postrobot.de>
//
// This file is part of fgms, the flightgear multiplayer server
//
// fgms is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// fgms is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with fgms.  If not, see <http://www.gnu.org/licenses/>
//

#if !defined(NDEBUG) && defined(_MSC_VER) // If MSVC, turn on ALL debug if Debug build
#define SG_DEBUG_ALL
#define SG_ENABLE_DEBUG
#endif // 

#ifndef SG_ENABLE_DEBUG
// #define SG_DEBUG_ALL
#define SG_ENABLE_DEBUG
#define SG_ENABLE_DEBUG_OUT
#endif

// FIXME: internal variables (m_LogFileName, m_ProtoMinorVersion, etc)
// should all be in the property tree (so they can be changed by the
// admin interface)

#include "fgms.hxx"
#include <fglib/fg_typcnvt.hxx>
#include <simgear/props/props_io.hxx>
#include <simgear/debug/debug.hxx>
#ifdef _MSC_VER
#include <getopt/getopt.h>
#endif

// bool         RunAsDaemon = true;
bool         RunAsDaemon = false;
#ifndef _MSC_VER
cDaemon      Myself;
#endif

//////////////////////////////////////////////////////////////////////
//
//	The version of fgms, syntax: MAJOR.MINOR.AGE
//
//	Increment AGE if the source code has changed at all,
//	i.e. bugfixes etc
//
//	If fgms has new features but is still protocol compatible
//	with older versions increment MINOR and set AGE to 0
//
//	If fgms gets incompatible with older versions increment
//	MAJOR and set MINOR and AGE to 0
//
//////////////////////////////////////////////////////////////////////
const char* SERVER_VERSION = "1.0.0-pre-alpha1";

//////////////////////////////////////////////////////////////////////

//
// Constrcutor initialises statiscal values to 0
//
Relay::Relay
()
{
	ID      = ( size_t ) -1;
	Time    = time ( 0 );                                                   
	Name    = "";
	Addr.Assign ( 0, 0 );
	PktsRcvd  = 0;
	PktsSent  = 0;
	BytesRcvd = 0;
	BytesSent = 0;
} // Relay::Relay ()

//////////////////////////////////////////////////////////////////////

//
// Update counters for sent data
//
void
Relay::UpdateSent
(
	size_t bytes
)
{
	PktsSent++;
	BytesSent += bytes;
} // Relay::UpdateSent ()

//////////////////////////////////////////////////////////////////////

//
// Update counters for received data
//
void
Relay::UpdateRcvd
(
	size_t bytes
)
{
	PktsRcvd++;
	BytesRcvd += bytes;
} // Relay::UpdateSent ()

//////////////////////////////////////////////////////////////////////

//
// Output server object on a standard stream
//
std::ostream&
operator <<
(
	std::ostream& o,
	Relay& R
)
{
	o << R.Name << " " << R.Location
	  << " " << R.Addr
	  << " (ID " << R.ID << ")";
	return ( o );
} // operator << ( Relay )

//////////////////////////////////////////////////////////////////////

//
// initialise a player
//
Pilot::Pilot
()
{
	ID      = ( size_t ) -1;
	Time    = time ( 0 );                                                   
	Name    = "";
	Addr.Assign ( 0, 0 );
	Origin	  = "";
	IsLocal	  = true;
	JoinTime  = time ( 0 );
	LastSeen  = JoinTime;
	LastSent  = 0;
	DoUpdate  = false;
	PktsRcvd  = 0;
	PktsSent  = 0;
	BytesRcvd = 0;
	BytesSent = 0;
	State	  = REGISTER;
	LastRelayedToInactive = 0;
} // Pilot::Pilot ()

//////////////////////////////////////////////////////////////////////

//
// Output a player object on a standard stream
//
std::ostream&
operator <<
(
	std::ostream& o,
	Pilot& P
)
{
	o << P.Name << " " << P.Addr << " (ID " << P.ID << ")";
	return ( o );
} // operator << ( Pilot )

//////////////////////////////////////////////////////////////////////

FGMS::FGMS
()
{
	m_WantExit	= false;
	m_NumClients	= 1;
	m_CheckInterval = 10;
	m_Properties.setIntValue ("/fgms/online_since", time ( 0 ) );
	sglog().setLogLevels ( SG_ALL, SG_WARN );
} // FGMS::FGMS()

//////////////////////////////////////////////////////////////////////

FGMS::~FGMS
()
{
	Done();
} // FGMS::~FGMS()

//////////////////////////////////////////////////////////////////////

//
//  Basic initialization, called on startup and if we received a
//  SIGHUP.
//  If we are already initialized, close all connections and
//  re-init all variables.
//
int
FGMS::Init
()
{
	int    Port = m_Properties.getIntValue    ( "/fgms/port" );
	string Addr = m_Properties.getStringValue ( "/fgms/addr" );

	if ( ! m_Peer.Listen ( Addr, Port ) )
	{
		return ( fgms::FAILED );
	}
	m_IamHUB = false;
	m_CheckInterval = m_Properties.getIntValue (
	  "/multiplayer/system/checkinterval" );
	m_Peer.SetClientType (fgms::FGMS);
	m_RequireAuth  = m_Properties.getBoolValue (
	  "/fgms/require_authentication" );
	m_RegisterFGLS = m_Properties.getBoolValue (
	  "/fgms/register_at_fgls" );
	m_OutOfReach = m_Properties.getIntValue ( "/fgms/out_of_reach" );
	m_ResendTime = m_Peer.GetResendTime ();
	SG_LOG ( SG_NETWORK, SG_ALERT, "# listening to port " << Port);
	m_State = RUNNING;
	if ( m_RegisterFGLS )
	{
		SG_LOG ( SG_NETWORK, SG_INFO,
		  "# trying to register at FGLS" );
		m_State = REGISTER;
		//////////////////////////////////////////////////
		// Setup FGLS
		//////////////////////////////////////////////////
		Addr = m_Properties.getStringValue ( "/fgls/addr" );
		Port = m_Properties.getIntValue    ( "/fgls/port" );
		if ( Addr == "" )
		{
			SG_LOG ( SG_NETWORK, SG_ALERT,
			  "Address of FGLS not set in config!" );
			return ( fgms::FAILED );
		}
		if ( Port == 0 )
		{
			SG_LOG ( SG_NETWORK, SG_ALERT,
			  "Port of FGLS not set in config!" );
			return ( fgms::FAILED );
		}
		m_FGLS.Addr.Assign ( Addr, Port );
	}
	else
	{
		SG_LOG ( SG_NETWORK, SG_INFO,
		  "# NOT trying to register at FGLS" );
		m_Peer.SetSenderID ( 1 );
	}
	sglog().enable_with_date ( true ); 
	return ( fgms::SUCCESS );
} // FGMS::Init()

//////////////////////////////////////////////////////////////////////

void
FGMS::PrintHelp
()
{
	cerr << endl;
	cerr << "syntax: fgms [options] configname" << endl;
	cerr << "\n"
	  "options are:\n"
	  "-h            print this help screen\n"
	  "-a PORT       listen to PORT for telnet\n"
	  "-p PORT       listen to PORT\n"
	  "-t TTL        Time a client is active while not sending packets\n"
	  "-o OOR        nautical miles two players must be apart to be out of reach\n"
	  "-l LOGFILE    Log to LOGFILE\n"
	  "-v LEVEL      verbosity (loglevel) in range 1 (much) and 4 (few)\n"
	  "              default is 3, 0 prints *allot*\n"
	  "-n NAME       name of this server\n"
	  "-d            do _not_ run as a daemon (stay in foreground)\n"
	  "-D            do run as a daemon\n"
	  "-s            standalone, don't use FGLS or FGAS\n"
	  "\n"
	  "the default is to run as a daemon, which can be overridden in the\n"
	  "config file.\n"
	  "commandline parameters always override config file options\n"
	  "\n";
} // PrintHelp ()


//////////////////////////////////////////////////////////////////////

int                                                                             
FGMS::ParseParams
(
	int argc,
	char* argv[]
)
{
	int     m;
	int     v;
	int     E;

	//
	// parse the commandline twice. First run to get the name
	// of the config file
	//
	while ( ( m=getopt ( argc,argv,"a:dDhl:n:o:p:t:v:s" ) ) != -1 )
	{
		switch ( m )
		{
		case 'h':
			PrintHelp ();
			return ( fgms::FAILED );
		case 'a':
		case 'd':
		case 'D':
		case 'l':
		case 'n':
		case 'o':
		case 'p':
		case 't':
		case 'v':
		case 's':
			break;
		default:
			PrintHelp ();
			return ( fgms::FAILED );
		}
	}
	if ( optind >= argc )
	{
		PrintHelp ();
		cerr << "Expected configfilename." << endl << endl;
		return ( fgms::FAILED );
	}
	if ( optind < argc-1 )
	{
		PrintHelp ();
		cerr << "Too much parameters, only expected configfilename."
		     << endl << endl;
		return ( fgms::FAILED );
	}
	if ( ReadConfig ( argv[optind], m_Properties ) == fgms::FAILED )
	{
		return ( fgms::FAILED );
	}
	//
	// Parse the commandline a second time, so that the options
	// overwrite the config file values
	//
	optind = 1; // reset index
	while ( ( m=getopt ( argc,argv,"a:dDhl:n:o:p:t:v:s" ) ) != -1 )
	{
		switch ( m )
		{
		case 'h':
			cerr << endl;
			cerr << "syntax: " << argv[0]
			     << " [options] configname" << endl;
			PrintHelp ();
			break; // never reached
		case 'a':
			v = StrToNum<int> ( optarg, E );
			if ( E )
			{
				cerr << "invalid value for TelnetPort: '"
				     << optarg << "'" << endl;
				return ( fgms::FAILED );
			}
			m_Properties.setIntValue ( "/fgms/telnet_port", v);
			break;
		case 'p':
			v = StrToNum<int> ( optarg, E );
			if ( E )
			{
				cerr << "invalid value for Port: '"
				     << optarg << "'" << endl;
				return ( fgms::FAILED );
			}
			m_Properties.setIntValue ( "/fgms/port", v);
			break;
		case 'n':
			m_Properties.setStringValue ( "/fgms/name", optarg );
			break;
		case 'o':
			v = StrToNum<int> ( optarg, E );
			if ( E )
			{
				cerr << "invalid value for OutOfReach: '"
				     << optarg << "'" << endl;
				return ( fgms::FAILED );
			}
			m_Properties.setIntValue ( "/fgms/out_of_reach", v );
			break;
		case 'v':
			v = StrToNum<int> ( optarg, E );
			if ( E )
			{
				cerr << "invalid value for LogLevel: '"
				     << optarg << "'" << endl;
				return ( fgms::FAILED );
			}
			sglog().setLogLevels ( SG_ALL, ( sgDebugPriority ) v);
			break;
		case 't':
			v = StrToNum<int> ( optarg, E );
			if ( E )
			{
				cerr << "invalid value for expire: '"
				     << optarg << "'" << endl;
				return ( fgms::FAILED );
			}
			m_Properties.setIntValue ( "/fgms/client_expires", v );
			break;
		case 'l':
			m_Properties.setStringValue ( "/fgms/logfile", optarg );
			break;
		case 'd':
			RunAsDaemon = false;
			break;
		case 'D':
			RunAsDaemon = true;
			break;
		case 's':
			m_Properties.setBoolValue (
			  "/fgms/require_authentication", false );
			m_Properties.setBoolValue (
			  "/fgms/register_at_fgls", false );
			break;
		default:
			cerr << endl << endl;
			PrintHelp ();
			exit ( 1 );
		} // switch ()
	} // while ()

	return ( 1 ); // success
} // FGMS::ParseParams()

//////////////////////////////////////////////////////////////////////

//
// Register ourself at FGLS
//
void
FGMS::Register
()
{
	static time_t LastRegister = 0;
	time_t now = time ( 0 );

	if ( ( now - LastRegister ) < m_CheckInterval )
		return;
	fgms::t_PropertyList SendProps;

	m_Properties.setIntValue    (
	  "/multiplayer/registration/clienttype", fgms::FGMS);
	m_Properties.setStringValue ( "/multiplayer/registration/name",
	  m_Properties.getStringValue ( "/fgms/name" ) );
	m_Properties.setStringValue (
	  "/multiplayer/registration/location",
	  m_Properties.getStringValue ( "/fgms/location" ) );
	m_Properties.setIntValue    ( "/multiplayer/registration/port",
	  m_Properties.getIntValue ( "/fgms/port" ) );
	SendProps.push_back ( fgms::REGISTER_SERVER );
	SendProps.push_back ( m_Prop2ID["/multiplayer/registration/clienttype"] );
	SendProps.push_back ( m_Prop2ID["/multiplayer/registration/name"] );
	SendProps.push_back ( m_Prop2ID["/multiplayer/registration/location"] );
	SendProps.push_back ( m_Prop2ID["/multiplayer/registration/port"] );
	SG_LOG ( SG_NETWORK, SG_INFO, "sending REGISTER_SERVER to FGLS" );
	m_Peer.SetTarget ( m_FGLS.Addr );
	WriteProps ( m_Peer, m_Properties, SendProps );
	m_Peer.Send ();
} // FGMS::Register ()

//////////////////////////////////////////////////////////////////////

void
FGMS::AddRelay
(
	const NetAddr& Sender
)
{
	Relay NewServer;
	string tmp;
	int E;
	uint64_t ID;
	relay_iterator It;

	tmp = m_Properties.getStringValue ( "/multiplayer/fgms/client_id" );
	ID = StrToNum<uint64_t> (tmp, E);
	if ( E )
	{
		SG_LOG ( SG_NETWORK, SG_ALERT,
		  "AddRelay: illegal client ID " << tmp );
		return;
	}
	NewServer.Addr.Assign (
	  m_Properties.getStringValue ( "/multiplayer/fgms/address" ),
	  m_Properties.getIntValue ( "/multiplayer/fgms/port" )
	);
	NewServer.Name = m_Properties.getStringValue (
	  "/multiplayer/fgms/name" );
	NewServer.Location = m_Properties.getStringValue (
	  "/multiplayer/fgms/location" );
	It = FindRelayByID (ID);
	if ( It != RelayList.end () )
	{
		SG_LOG ( SG_NETWORK, SG_DEBUG,
		  "have already: " << NewServer.Addr.ToString() );
		return;
	}
	NewServer.ID = ID;
	RelayList.push_back ( NewServer );
	SG_LOG ( SG_NETWORK, SG_WARN, "new relay: "
	  << NewServer.Name << " " << NewServer.Location
	  << " ID " << NewServer.ID
	  << " " << NewServer.Addr.ToString() << ":" << NewServer.Addr.Port()
	);
	// only for debugging, remove later
	SG_LOG ( SG_NETWORK, SG_INFO, "My relay list is:");
	for ( It = RelayList.begin(); It != RelayList.end(); It++ )
	{
		SG_LOG ( SG_NETWORK, SG_INFO, *It );
	}
}

//////////////////////////////////////////////////////////////////////

void
FGMS::RemoveRelay
()
{
	relay_iterator It;
	string tmp;
	int E;

	tmp = m_Properties.getStringValue (
	  "/multiplayer/fgms/client_id" );
	uint64_t ID = StrToNum<uint64_t> (tmp, E);
	if ( E )
	{
		SG_LOG ( SG_NETWORK, SG_ALERT, "FGMS::RemoveRelay: "
		  << "Illegal ID " << ID
		);
		return;
	}
	It = FindRelayByID ( ID );
	if ( It == RelayList.end () )
	{
		SG_LOG ( SG_NETWORK, SG_ALERT, "FGMS::RemoveRelay: "
		  << "I should remove server ID " << ID
		  << " but have it not in my list!"
		);
		return;
	}
	SG_LOG ( SG_NETWORK, SG_WARN, *It << " is leaving the network." );
	It = RelayList.erase ( It );
	// only for debugging, remove later
	SG_LOG ( SG_NETWORK, SG_INFO, "My relay list is:");
	for ( It = RelayList.begin(); It != RelayList.end (); It++ )
	{
		SG_LOG ( SG_NETWORK, SG_INFO, "#" << *It );
	}
} // FGMS::RemoveRelay ()

//////////////////////////////////////////////////////////////////////

FGMS::pilot_iterator
FGMS::RemoveFGFS
(
	FGMS::pilot_iterator It
)
{
	if ( It == PilotList.end () )
	{	// should never happen
		return ( It );
	}
	SG_LOG ( SG_NETWORK, SG_ALERT, *It << " is leaving the network." );
	// TODO: tell all servers
	return ( PilotList.erase ( It ) );
} // FGMS::RemoveFGFS ( It )

//////////////////////////////////////////////////////////////////////


void
FGMS::RemoveFGFS
(
	uint64_t ID
)
{
	pilot_iterator It;

	It = FindPilotByID ( ID );
	if ( It == PilotList.end () )
	{
		SG_LOG ( SG_NETWORK, SG_ALERT, "FGMS::RemoveFGFS: "
		  << "I should remove client ID " << ID
		  << " but have it not in my list!"
		);
		return;
	}
	RemoveFGFS ( It );
} // FGMS::RemoveFGFS ( ID )

//////////////////////////////////////////////////////////////////////

void
FGMS::SendError
(
	const NetAddr & Sender,
	const uint32_t Cmd,
	const string & Msg
)
{
	fgms::t_PropertyList SendProps;

	SendProps.push_back ( Cmd );
	SendProps.push_back ( m_Prop2ID["/multiplayer/system/errormessage"] );
	m_Properties.setStringValue (
	  "/multiplayer/system/errormessage", Msg.c_str() );
	m_Peer.SetTarget (Sender);
	WriteProps (m_Peer, m_Properties, SendProps );
	SG_LOG ( SG_NETWORK, SG_DEBUG, "sending " << Msg << "..." );
	m_Peer.Send ();
} // FGMS::SendError ()

//////////////////////////////////////////////////////////////////////

//
// We received a register request from a client. Add the client to
// our list and sent an authentication request to FGAS
//
void
FGMS::RegisterFGFS
(
	const NetAddr& Sender
)
{
	Pilot	NewPilot;
	uint64_t SenderID;
	fgms::t_PropertyList SendProps;
	pilot_iterator It;

// FIXME: local or remote client?
	NewPilot.Type = (fgms::eCLIENTTYPES) m_Properties.getIntValue (
	  "/multiplayer/registration/clienttype" );
	NewPilot.Name = m_Properties.getStringValue (
	  "/multiplayer/registration/callsign" );
	NewPilot.Password = m_Properties.getStringValue (
	  "/multiplayer/registration/password" );
	//////////////////////////////////////////////////
	// check properties
	//////////////////////////////////////////////////
	if ( NewPilot.Type != fgms::FGFS  )
	{
		SendError (Sender, fgms::GENERAL_ERROR, "invalid client type");
		return;
	}
	if ( NewPilot.Name == "" )
	{
		SendError (Sender, fgms::GENERAL_ERROR, "invalid callsign");
		return;
	}
	if ( ( m_RequireAuth == true ) && ( NewPilot.Password == "" ) )
	{
		SendError (Sender, fgms::GENERAL_ERROR,
		  "authentication enforced: invalid password");
		return;
	}
	NewPilot.Addr = Sender;
	It = FindPilotByName ( NewPilot.Name );
	if ( It != PilotList.end () )
	{
		if ( It->Addr != Sender ) 
		{
			SendError (Sender, fgms::GENERAL_ERROR,
			  "You are already online!");
			return;
		}
		SenderID = It->ID;
		SG_LOG ( SG_NETWORK, SG_INFO,
		  "Know FGFS already: " << NewPilot.Name );
	}
	else
	{
		NewPilot.State = Pilot::REGISTER;
		m_NumClients++;
		SenderID = (m_Peer.GetSenderID() << 32) + m_NumClients;
		NewPilot.ID = SenderID;
		PilotList.push_back ( NewPilot );
		It = FindPilot ( NewPilot );
		SG_LOG ( SG_NETWORK, SG_WARN, "New Client: " << NewPilot );
	}
	It->Properties.setIntValue (
	  "/multiplayer/registration/clienttype", NewPilot.Type );
	It->Properties.setStringValue (
	  "/multiplayer/registration/callsign", NewPilot.Name.c_str() );
	It->Properties.setStringValue (
	  "/multiplayer/registration/password", NewPilot.Password.c_str() );
	It->Properties.setStringValue (
	  "/multiplayer/registration/client_id",
	  NumToStr<uint64_t> ( SenderID, 0 ).c_str() );
	//////////////////////////////////////////////////
	// if the client is sending a registration
	// request, it is not authenticated yet. So send
	// auth request if configured
	//////////////////////////////////////////////////
	if ( m_RequireAuth == true )
	{
		if ( m_FGAS.IsValid == false )
		{
			SG_LOG ( SG_NETWORK, SG_ALERT,
			  "I'm required to authenticate users but have no FGAS!"
			);
			SendError (Sender, fgms::GENERAL_ERROR,
			  "authentication enforced: no auth server available!");
			return;
		}
		SendProps.push_back ( fgms::AUTH_CLIENT );
		SendProps.push_back (
		  m_Prop2ID["/multiplayer/registration/callsign"] );
		SendProps.push_back (
		  m_Prop2ID["/multiplayer/registration/password"] );
		SendProps.push_back (
		  m_Prop2ID["/multiplayer/registration/client_id"] );
		SG_LOG ( SG_NETWORK, SG_INFO,
		  "requesting authentication for " << NewPilot.Name);
		m_Peer.SetTarget ( m_FGAS.Addr );
		WriteProps (m_Peer, It->Properties, SendProps );
		m_Peer.Send ();
		SendProps.clear ();
	}
	else
	{
		SendProps.push_back ( fgms::AUTHENTICATED );
		SendProps.push_back (
		  m_Prop2ID["/multiplayer/system/update_frequency"] );
		It->Properties.setIntValue (
		  "/multiplayer/system/update_frequency", 10 );
	}
	//////////////////////////////////////////////////
	// tell the registering client about the
	// successfull registration
	//////////////////////////////////////////////////
	SendProps.push_back ( fgms::REGISTER_OK );
	SendProps.push_back ( fgms::SET_CLIENT_ID );
	SendProps.push_back (
	  m_Prop2ID["/multiplayer/registration/client_id"] );
	SG_LOG ( SG_NETWORK, SG_INFO, "sending REGISTER_OK to "
	  << NewPilot.Name );
	m_Peer.SetTarget    ( Sender );
	WriteProps (m_Peer, It->Properties, SendProps );
	m_Peer.Send ();
	// reset registration properties
	m_Properties.setIntValue    ( "/multiplayer/registration/clienttype",
	  fgms::NOCLIENT);
	m_Properties.setStringValue ( "/multiplayer/registration/callsign", "");
	m_Properties.setStringValue ( "/multiplayer/registration/password", "");
	m_Properties.setStringValue ( "/multiplayer/registration/address", "");
	m_Properties.setStringValue ( "/multiplayer/registration/client_id","");
	m_Properties.setIntValue    ( "/multiplayer/registration/port", 0);
} // FGMS::RegisterFGFS ()

//////////////////////////////////////////////////////////////////////

//
// We received an answer to our authentication request to FGAS on
// behalf of a client
//
void
FGMS::HandleAuthOK
()
{
	int E;
	uint64_t ID;
	fgms::t_PropertyList SendProps;
	pilot_iterator It;

	ID = StrToNum<uint64_t> ( m_Properties.getStringValue (
	  "/multiplayer/registration/client_id" ), E
	);
	It = FindPilotByID (ID);
	if ( It == PilotList.end () )
	{
		SG_LOG ( SG_NETWORK, SG_ALERT,
		  "I got an AUTH_OK for ID " << ID
		  << " but that ID is unknown!" );
		return;
	}
	SG_LOG ( SG_NETWORK, SG_DEBUG,
	  "I got an AUTH_OK for " << *It );
	It->State = Pilot::LOCALLY_AUTHENTICATED;
	SendProps.push_back ( fgms::AUTHENTICATED );
	SendProps.push_back (
	  m_Prop2ID["/multiplayer/system/update_frequency"] );
	m_Properties.setIntValue (
	  "/multiplayer/system/update_frequency", 10 );
	SG_LOG ( SG_NETWORK, SG_INFO, "sending AUTHENTICATED to "
	  << It->Name );
	m_Peer.SetTarget ( It->Addr );
	WriteProps (m_Peer, m_Properties, SendProps );
	m_Peer.Send ();
	m_Properties.setStringValue ( "/multiplayer/registration/client_id", "");
} // FGMS::HandleAuthOK ()

//////////////////////////////////////////////////////////////////////

//
// We received an AUTH_FAILED to our authentication request to FGAS on
// behalf of a client
//
void
FGMS::HandleAuthFailed
()
{
	int E;
	uint64_t ID;
	fgms::t_PropertyList SendProps;
	pilot_iterator It;

	ID = StrToNum<uint64_t> ( m_Properties.getStringValue (
	  "/multiplayer/registration/client_id" ), E
	);
	It = FindPilotByID (ID);
	if ( It == PilotList.end () )
	{
		SG_LOG ( SG_NETWORK, SG_ALERT,
		  "I got an AUTH_FAILED for ID " << ID
		  << " but that ID is unknown!" );
		return;
	}
	SG_LOG ( SG_NETWORK, SG_ALERT,
	  "I got an AUTH_FAILED for " << *It );
	SendProps.push_back ( fgms::AUTH_FAILED );
	SendProps.push_back ( m_Prop2ID["/multiplayer/system/errormessage"] );
	SG_LOG ( SG_NETWORK, SG_ALERT, "sending AUTH_FAILED to " << It->Name );
	m_Peer.SetTarget ( It->Addr );
	WriteProps (m_Peer, m_Properties, SendProps );
	m_Peer.Send ();
	It = PilotList.erase ( It );
	m_Properties.setStringValue ( "/multiplayer/registration/client_id", "");
} // FGMS::HandleAuthFailed ()

//////////////////////////////////////////////////////////////////////

void
FGMS::HandleCommand
(
	int Command,
	const NetAddr& Sender,
	t_ReceivePacket* Packet
)
{
	switch ( Command )
	{
	case fgms::REGISTER_OK:
		SG_LOG ( SG_NETWORK, SG_ALERT,
		  "Registered with FGLS, my id is " << m_Peer.GetSenderID () );
		m_FGLS.LastSeen = time ( 0 );
		m_FGLS.RegisteredAt = m_FGLS.LastSeen;
		m_State = RUNNING;
		break;
	case fgms::REGISTER_FGFS:
		RegisterFGFS ( Sender );
		break;
	case fgms::REGISTER_AGAIN:
		m_State = REGISTER;
		break;
	case fgms::GENERAL_ERROR:
		SG_LOG ( SG_NETWORK, SG_ALERT,
		  m_Properties.getStringValue (
		    "/multiplayer/system/errormessage") );
		exit (1 );
		break;
	case fgms::SET_CLIENT_ID:
		// nothing to do
		m_NumClients = m_Peer.GetSenderID() << 32;
		break;
	case fgms::YOU_ARE_HUB:
		SG_LOG ( SG_NETWORK, SG_WARN, "I am HUB!" );
		m_IamHUB = true;
		RelayList.clear ();
		break;
	case fgms::SERVER_QUIT:
		RemoveRelay ();
		break;
	case fgms::FGFS_QUIT:
		// RemoveFGFS ( Packet->SenderID );
		SG_LOG ( SG_NETWORK, SG_ALERT, "fgms::FGFS_QUIT: ??" );
		break;
	case fgms::YOU_ARE_LEAVE:
		m_IamHUB = false;
		RelayList.clear ();
		SG_LOG ( SG_NETWORK, SG_WARN, "my HUB is "
	 	  << m_Properties.getStringValue (
		    "/multiplayer/fgms/name" )
		);
		AddRelay ( Sender );
		break;
	case fgms::NEW_SERVER:
		AddRelay ( Sender );
		break;
	case fgms::USE_FGAS:
		m_FGAS.Addr.Assign (
			m_Properties.getStringValue (
			  "/multiplayer/fgas/address" ),
			m_Properties.getIntValue    (
			  "/multiplayer/fgas/port" )
		);
		m_FGAS.IsValid = true;
		m_FGAS.RegisteredAt = time ( 0 );
		SG_LOG ( SG_NETWORK, SG_WARN, "using FGAS " << m_FGAS.Addr );
		break;
	case fgms::NO_FGAS:
		SG_LOG ( SG_NETWORK, SG_ALERT, "no FGAS available" );
		break;
	case fgms::AUTH_OK:
		HandleAuthOK ();
		break;
	case fgms::AUTH_FAILED:
		HandleAuthFailed ();
		break;
	case fgms::PING:
		if ( Sender == m_FGLS.Addr )
		{
			SG_LOG ( SG_NETWORK, SG_INFO,
			  "got PING from FGLS " << m_FGLS.Addr );
			m_FGLS.LastSeen = time ( 0 );
			m_FGLS.NumPingsRcvd++;
		}
		else
		{
			SG_LOG ( SG_NETWORK, SG_INFO,
			  "got PING from Pilot? " << Sender );
		}
		break;
	case fgms::PONG:
		SG_LOG ( SG_NETWORK, SG_INFO,
		  "# # got PONG from " << Sender );
		HandlePong (Sender, Packet);
		break;
	default:
		SG_LOG ( SG_NETWORK, SG_ALERT,
		  "unhandled command: " << Command );
	}
} // FGMS::HandleCommand ()

//////////////////////////////////////////////////////////////////////

void
FGMS::HandlePong
(
	const NetAddr& Sender,
	t_ReceivePacket* Packet
)
{
	time_t now = time( 0 );
	time_t ts  = m_Properties.getIntValue (
	  "/multiplayer/system/pong_timestamp" );
	time_t diff = now - ts;
	pilot_iterator It;

	if ( Sender == m_FGLS.Addr )
	{
		SG_LOG ( SG_NETWORK, SG_INFO,
		  "got PONG from FGLS " << m_FGLS.Addr );
		m_FGLS.LastSeen = now;
		m_FGLS.NumPongs++;
		return;
	}
	It = FindPilotByID ( Packet->SenderID );
	if ( It == PilotList.end () )
	{
		SG_LOG ( SG_NETWORK, SG_ALERT, "got PONG from unknown ID "
		  << Packet->SenderID );
		return;
	}
	else
	{
		SG_LOG ( SG_NETWORK, SG_INFO,
		  "got PONG from Pilot " << *It );
	}
	SG_LOG ( SG_NETWORK, SG_ALERT, "got PONG from "
	  << It->Name << " " << It->ID
	  << " after " << diff << " seconds" );
	It->LastSeen = now;
} // FGMS::HandlePong ()

//////////////////////////////////////////////////////////////////////

void
FGMS::HandlePilot
(
	t_ReceivePacket* Packet
)
{
	pilot_iterator It;
	fgms::t_PropertyList RcvdProps;
	fgms::t_PropertyList SendProps;

	It = FindPilotByID ( Packet->SenderID );
	if ( It == PilotList.end () )
	{
		SG_LOG ( SG_NETWORK, SG_ALERT, "should know pilot ID "
		     << Packet->SenderID
		     << " but have not found it" );
		SendProps.push_back ( fgms::REGISTER_AGAIN );
		m_Peer.SetTarget ( Packet->Sender );
		WriteProps (m_Peer, m_Properties, SendProps );
		m_Peer.Send ();
		return;
	}
	ParsePacket ( Packet, m_Peer, RcvdProps, It->Properties );
	m_FGLS.LastSeen = time ( 0 );
	m_FGLS.NumPingsRcvd++;
	while ( RcvdProps.size () > 0 )
	{
		int Command = RcvdProps.front ();
		RcvdProps.pop_front ();
		if (Command < fgms::INTERNAL_COMMAND)
		{
			switch ( Command )
			{
			case fgms::PING:
				SG_LOG ( SG_NETWORK, SG_INFO,
				  "got PING from Pilot! " << *It );
				It->LastSeen = time ( 0 );
				It->NumPingsRcvd++;
				break;
			case fgms::PONG:
				SG_LOG ( SG_NETWORK, SG_INFO,
				  "got PONG from Pilot! " << *It );
				It->LastSeen = time ( 0 );
				It->NumPongs++;
				break;
			case fgms::FGFS_QUIT:
				It = RemoveFGFS ( It );
				return;
			case fgms::AUTH_FAILED:
				HandleAuthFailed ();
				break;
			default:
				SG_LOG ( SG_NETWORK, SG_ALERT,
				  "Pilot: unhandled command: " << Command );
			}
		}
	}
//	writeProperties (cout, & It->Properties, true);
} // FGMS::HandlePilot ()

//////////////////////////////////////////////////////////////////////

void
FGMS::Maintainance
()
{
	fgms::t_PropertyList SendProps;
	time_t now = time ( 0 );
	time_t timeout = m_CheckInterval * 2; // miss 3 pings
	time_t diff;

	SendProps.push_back ( fgms::PING );
	//////////////////////////////////////////////////
	// PING fgls
	//////////////////////////////////////////////////
	if ( m_RegisterFGLS == true )
	{
		diff = now - m_FGLS.LastSeen;
		if ( diff > timeout )
		{	// register again
			m_State = REGISTER;
			SG_LOG ( SG_NETWORK, SG_ALERT, "FLGS: last seen "
			  << now - m_FGLS.LastSeen
			  << " seconds ago, registering again"
			);
			return;
		}
		else if ( diff >= m_CheckInterval )
		{
			SG_LOG ( SG_NETWORK, SG_INFO, "sending Ping to FLGS: "
			  << m_FGLS.Addr );
			m_Peer.SetTarget ( m_FGLS.Addr );
			WriteProps (m_Peer, m_Properties, SendProps );
			m_FGLS.NumPingsSent++;
			m_Peer.Send ();
		}
	}
	//////////////////////////////////////////////////
	// PING all pilots
	//////////////////////////////////////////////////
	pilot_iterator It;
	for ( It = PilotList.begin (); It != PilotList.end(); It++ )
	{
		SG_LOG ( SG_NETWORK, SG_INFO, "doing Pilot: " << *It );
		diff = now - It->LastSeen;
		if ( diff > timeout )
		{
			SG_LOG ( SG_NETWORK, SG_ALERT, *It
			  << " last seen " << diff << " seconds ago!" );
			It = RemoveFGFS ( It );
			continue;
		}
		SG_LOG ( SG_NETWORK, SG_INFO, "sending Ping to Pilot "
		  << *It );
		m_Peer.SetTarget ( It->Addr );
		WriteProps (m_Peer, m_Properties, SendProps );
		m_Peer.Send ();
		It->NumPingsSent++;
	}
} // FGMS::Maintainance ()

//////////////////////////////////////////////////////////////////////

void
FGMS::HandleRequest
()
{
	NetAddr Sender;
	t_ReceivePacket* Packet;
	fgms::t_PropertyList RcvdProps;

	struct Converter
	{
	#if (__BYTE_ORDER == __LITTLE_ENDIAN)
		uint32_t ServerID;
		uint32_t PilotID;
	#else
		uint32_t PilotID;
		uint32_t ServerID;
	#endif
	};

	m_Peer.Receive ();
	while ( m_Peer.HasPackets() )
	{
		Sender = m_Peer.GetSender ();
		try
		{
			Packet = m_Peer.NextPacket ();
		}
		catch ( sg_exception & ex )
		{
			SendError ( Sender, fgms::GENERAL_ERROR,
			  ex.getFormattedMessage () );
			continue;
		}
		if ( Packet == 0 )
		{
			SG_LOG ( SG_NETWORK, SG_ALERT, "no packet" );
			continue;
		}
		Converter* ID = (Converter*) &Packet->SenderID;
		if (ID->PilotID != 0)
		{	// packet from registered pilot
			HandlePilot ( Packet );
			continue;
		}
		if (Packet->SenderID == 0)
		{	// packet from unregistered pilot
			ParsePacket ( Packet, m_Peer, RcvdProps, m_Properties );
		}
		else if ( (ID->ServerID != 0) && (ID->PilotID == 0) )
		{	// packet from registered server
			ParsePacket ( Packet, m_Peer, RcvdProps, m_Properties );
		}
		else
		{
			cout << "Moooooooooooooooooeeeeeeeeeeeeeeeepppppppppp" << endl;
		}
		while ( RcvdProps.size () > 0 )
		{
			int Command = RcvdProps.front ();
			RcvdProps.pop_front ();
			if (Command < fgms::INTERNAL_COMMAND)
			{
				HandleCommand (Command, Sender, Packet);
			}
		}
	}
} // FGMS::HandleRequest ()

//////////////////////////////////////////////////////////////////////
//
//      main loop of the server
//
//////////////////////////////////////////////////////////////////////
int
FGMS::Loop
()
{
	int Client;
	time_t now;
	static time_t LastCheck = time ( 0 );

	while ( m_WantExit == false )
	{
		if (m_State == REGISTER)
		{
			Register ();
		}
		Client = m_Peer.WaitForClients (m_ResendTime);
		if (Client == UDP_Peer::ne_TIMEOUT)
		{
			// SG_LOG ( SG_NETWORK, SG_DEBUG, "sending queue..." );
			// m_Peer.Send ();
		}
		else if (Client < 0)
		{
			SG_LOG ( SG_NETWORK, SG_ALERT,
			  "fgms::Loop() - Bytes <= 0!");
			perror("### ");
			continue;
		}
		else if (Client > 0)
		{
			HandleRequest ();
		}
		now = time ( 0 );
		if ( ( now - LastCheck ) >= m_CheckInterval )
		{
			Maintainance ();
			LastCheck = now;
		}
	}
	return ( fgms::SUCCESS );
} // FGMS::Loop()

//////////////////////////////////////////////////////////////////////
//
//      open the logfile
//
//////////////////////////////////////////////////////////////////////
void
FGMS::OpenLogfile
()
{
	if ( m_LogFile )
	{
		m_LogFile.close ();
	}
	m_LogFile.open ( m_Properties.getStringValue ( "/fgms/logfile" ),
	  ios::out|ios::app );
	sglog().enable_with_date ( true );
	sglog().set_output ( m_LogFile );
} // FGMS::OpenLogfile ()

//////////////////////////////////////////////////////////////////////

void
FGMS::WantExit
()
{
	m_WantExit = true;
}

//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//
//      close sockets, logfile etc.
//
//////////////////////////////////////////////////////////////////////
void
FGMS::Done
()
{
	if ( ( m_State != RUNNING ) || ( m_RegisterFGLS == false ) )
		return;

	fgms::t_PropertyList SendProps;

	// FIXME: write QUIT message to all clients
	SendProps.push_back ( fgms::SERVER_QUIT );
	m_Peer.SetTarget ( m_FGLS.Addr );
	WriteProps (m_Peer, m_Properties, SendProps );
	m_Peer.Send ();
} // FGMS::Done()

//////////////////////////////////////////////////////////////////////

FGMS::relay_iterator
FGMS::FindRelayByID
(
	const uint64_t & ID
)
{
	relay_iterator It;

	for ( It = RelayList.begin(); It != RelayList.end(); It++ )
	{
		if ( It->ID == ID )
		{
			return It;
		}
	}
	return It;
} // FGMS::FindRelayByID ()

//////////////////////////////////////////////////////////////////////

FGMS::pilot_iterator
FGMS::FindPilot
(
	const Pilot & Pilot
)
{
	pilot_iterator It;

	for ( It = PilotList.begin(); It != PilotList.end(); It++ )
	{
		if ( *It == Pilot )
		{
			return It;
		}
	}
	return It;
} // FGMS::FindPilot ()

//////////////////////////////////////////////////////////////////////

FGMS::pilot_iterator
FGMS::FindPilotByID
(
	const uint64_t & ID
)
{
	pilot_iterator It;

	for ( It = PilotList.begin(); It != PilotList.end(); It++ )
	{
		if ( It->ID == ID )
		{
			return It;
		}
	}
	return It;
} // FGMS::FindPilotByID ()

//////////////////////////////////////////////////////////////////////

FGMS::pilot_iterator
FGMS::FindPilotByName
(
	const std::string & Name
)
{
	pilot_iterator It;

	for ( It = PilotList.begin(); It != PilotList.end(); It++ )
	{
		if ( It->Name == Name )
		{
			return It;
		}
	}
	return It;
} // FGMS::FindPilotByName ()

//////////////////////////////////////////////////////////////////////

FGMS            Server;
extern  bool    RunAsDaemon;
extern  cDaemon Myself;

//////////////////////////////////////////////////
// Signal handling is only used for debugging
// purposes. I'm a bit irritated that the
// destructors are not called if the program
// is interrupted by ^C !?
//////////////////////////////////////////////////
void
SigHandler
(
        int s
)
{
	SG_LOG ( SG_NETWORK, SG_ALERT, "caught signal " << s );
	SG_LOG ( SG_NETWORK, SG_ALERT, "exiting... " );
	Server.WantExit ();
	signal ( s, SigHandler );
} // SigCHLDHandler ()

//////////////////////////////////////////////////////////////////////
//
//      MAIN routine
//
//////////////////////////////////////////////////////////////////////
int
main
(
        int argc,
        char* argv[]
)
{
	// catch SIGINT
	signal ( SIGINT, SigHandler );
	if ( Server.ParseParams ( argc, argv ) == fgms::FAILED )
	{
		return ( fgms::FAILED  );
	}
	if ( ! Server.Init () )
	{
		return ( fgms::FAILED  );
	}
	if ( RunAsDaemon )
	{
		Myself.Daemonize ();
	}
	Server.Loop();
	return ( 0 );
} // main()


