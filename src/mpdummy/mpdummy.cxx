/// @file mpdummy.cxx                                                                                                     
/// A reference implementation of a client connected to the
/// flightgear multiplayer network.
///
/// @author     Oliver Schroeder <fgms@o-schroeder.de>
/// @date       2008-2015
/// @copyright  GPLv3
///
/// This is a reference client for the flightgear multiplayer network.
/// It serves as an example of how to implement a client paricipating in
/// the network, as well as a test client for server development.
///

// Copyright (C) Oliver Schroeder <fgms@postrobot.de>
//
// This file is part of fgms, the flightgear multiplayer server
//
// fgms is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// fgms is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with fgms.  If not, see <http://www.gnu.org/licenses/>
//

#define MPDUMMY_VERSION "0.0.1-pre-alpha1"

#include <signal.h>
#include <fglib/fg_list.hxx>
#include <fglib/fg_typcnvt.hxx>
#include <flightgear/Network/fg_proto.hxx>
#ifdef _MSC_VER
#include <getopt/getopt.h>
#endif
using namespace std;

class mpdummy : public ProtocolHandler
{
public:
	mpdummy ();
	~mpdummy ();
	int  Init ();
	void Loop ();
	void Done ();
	void WantExit ();
	int  ParseParams ( int argc, char* argv[] );
private:
	enum eSTATE
	{
		STARTING,
		REQUEST_SERVER,
		REGISTER,
		WAIT_AUTH,
		RUNNING
	};
	class FGMS
	{
	public:
		time_t		LastSeen;
		time_t		RegisteredAt;
		uint64_t	NumPingsSent;
		uint64_t	NumPingsRcvd;
		uint64_t	NumPongs;
		NetAddr		Addr;
	};
	void PrintHelp ();
	void RequestServer ();
	void Register ();
	void SendMyData ();
	void HandleCommand ( int Command, const NetAddr& Sender,
	  t_ReceivePacket* Packet );
	void HandleRequest ();
	void DumpProps ( const char* Name, fgms::t_PropertyList& Props );
	eSTATE	m_State;
	FGMS	m_FGMS;
	int	m_UpdateFrequency;
	bool	m_WantExit;
}; // class mpdummy

//////////////////////////////////////////////////////////////////////

mpdummy::mpdummy
()
{
	m_State		= STARTING;
	m_Properties.setIntValue ( "/mpdummy/log_level", SG_WARN );
	sglog().setLogLevels ( SG_ALL, SG_BULK );
} // mpdummy::mpdummy ()

//////////////////////////////////////////////////////////////////////

mpdummy::~mpdummy
()
{
	Done ();
} // mpdummy::~mpdummy ()

//////////////////////////////////////////////////////////////////////

void
mpdummy::DumpProps ( const char* Name, fgms::t_PropertyList& Props )
{
	list<uint32_t>::iterator i;

	cout << "### " << Name << " ###" << endl;
	for (i=Props.begin(); i!=Props.end(); i++)
	{
		cout << *i << " " << m_ID2Prop[ *i ] << endl;
	}
	cout << endl;
}

//////////////////////////////////////////////////////////////////////

int
mpdummy::Init
()
{
	string	MyAddr;
	int	MyPort;

	m_OneTimeProperties.push_front ( fgms::REGISTER_FGFS );
	m_WantExit = false;
	m_Properties.setIntValue (
	  "/multiplayer/registration/clienttype", fgms::FGFS);
	MyAddr = m_Properties.getStringValue (
	  "/multiplayer/registration/addr" );
	MyPort = m_Properties.getIntValue    (
	  "/multiplayer/registration/port" );
	m_UpdateFrequency = 10;
	if (! m_Peer.Listen ( MyAddr, MyPort ) )
	{
		return (fgms::FAILED);
	}
	m_Peer.SetClientType (fgms::FGFS);
	if ( m_State == STARTING )
		m_State = REQUEST_SERVER;
//#if 0
	DumpProps ( "once",   m_OneTimeProperties );
	DumpProps ( "to send",   m_ToSendProperties );
	DumpProps ( "on change", m_OnChangeProperties );
//#endif
	m_FGMS.LastSeen		= 0;
	m_FGMS.RegisteredAt	= 0;
	m_FGMS.NumPingsRcvd	= 0;
	m_FGMS.NumPingsSent	= 0;
	m_FGMS.NumPongs		= 0;
	SG_LOG ( SG_NETWORK, SG_INFO, "### listening to port " << MyPort
	  << " update frequency " << m_UpdateFrequency << "hz"
	);
	return (fgms::SUCCESS);
} // mpdummy::Init ()

//////////////////////////////////////////////////////////////////////

void
mpdummy::Done
()
{
	if ( m_State != RUNNING )
		return;
	SG_LOG ( SG_NETWORK, SG_WARN,
	  "another happy customers is leaving the building...");
	fgms::t_PropertyList SendProps;
	m_Peer.SetTarget ( m_FGMS.Addr );
	SendProps.push_back ( fgms::FGFS_QUIT );
	WriteProps (m_Peer, m_Properties, SendProps );
	m_Peer.Send ();
} // mpdummy::Done ()

//////////////////////////////////////////////////////////////////////

void
mpdummy::PrintHelp
()
{
	cerr << "\n"
		"syntax: mpdummy [options] configname\n\n"
		"options are:\n"
		"-h            print this help screen\n"
		"-p PORT       listen to PORT\n"
		"-s SERVER-IP  connect to SERVER-IP\n"
		"-P PORT       use server on PORT\n"
		"-v LEVEL  verbosity (loglevel) in range 1 (much) and 4 (few)\n"
		"          default is 3, 0 prints *allot*\n"
		"\n";
} // mpdummy::PrintHelp ()

//////////////////////////////////////////////////////////////////////

int
mpdummy::ParseParams
(
	int   argc,
	char* argv[]
)
{
	int m;
	int e;

	//
	// parse the commandline twice. First run to get the name
	// of the config file
	//
	while ( ( m=getopt ( argc, argv, "hp:s:P:v:" ) ) != -1 )
	{
		switch ( m )
		{
		case 'h':
			PrintHelp ();
			return ( fgms::FAILED );

		case 'p':
		case 's':
		case 'P':
		case 'v':
			break;
		default:
			cerr << endl;
			return ( fgms::FAILED );
		}
	}
	if ( optind >= argc )
	{
		PrintHelp ();
		cerr << "Expected configfilename." << endl;
		return ( fgms::FAILED );
	}
	if ( optind < argc-1 )
	{
		PrintHelp ();
		cerr << "Too much parameters, only expected configfilename."
		     << endl;
		return ( fgms::FAILED );
	}
	m_Properties.setStringValue ( "/mpdummy/config_name", argv[optind] );
	if ( ReadConfig ( argv[optind], m_Properties ) == fgms::FAILED )
	{
		return (fgms::FAILED);
	}
	//
	// Parse the commandline a second time, so that the
	// options overwrite the config file values
	//
	optind = 1; // reset index
	int v;
	while ( ( m=getopt ( argc, argv, "p:s:P:v:" ) ) != -1 )
	{
		switch ( m )
		{
		case 'p':
			v = StrToNum<int> ( optarg, e );
			if ( e )
			{
				cerr << "invalid value for listen port: '"
				     << optarg << "'" << endl;
				return ( fgms::FAILED );
			}
			m_Properties.setIntValue ( "/multiplayer/registration/port", v );
			break;
		case 's':
			m_FGMS.Addr.Assign ( optarg );
			m_State = REGISTER; // don't request server
			break;
		case 'P':
			v = StrToNum<int> ( optarg, e );
			if ( e )
			{
				cerr << "invalid value for port: '"
				     << optarg << "'" << endl;
				return ( fgms::FAILED );
			}
			m_FGMS.Addr.SetPort ( v );
			break;
		case 'v':
			v = StrToNum<int> ( optarg, e );
			if ( e )
			{
				cerr << "invalid value for LogLevel: '"
				     << optarg << "'" << endl;
				return ( fgms::FAILED );
			}
			m_Properties.setIntValue ( "/mpdummy/log_level", v );
			sglog().setLogLevels ( SG_ALL, ( sgDebugPriority ) v);
			break;
		default:
			PrintHelp ();
			return ( fgms::FAILED );
		}
	}
	if ( optind >= argc )
	{
		PrintHelp ();
		cerr << "Expected configfilename." << endl;
		return ( fgms::FAILED );
	}
	if ( optind < argc-1 )
	{
		PrintHelp ();
		cerr << "Too much parameters, only expected configfilename."
		     << endl;
		return ( fgms::FAILED );
	}

	return ( fgms::SUCCESS );
} // mpdummy::ParseParams ()

//////////////////////////////////////////////////////////////////////

void
mpdummy::RequestServer
()
{
	static int request_count = 0;

	request_count++;
	if ( request_count > 3 )
	{
		SG_LOG ( SG_NETWORK, SG_WARN,
		  "No response to REQUEST_SERVER, giving up..." );
		m_WantExit = true;
		return;
	}
	fgms::t_PropertyList SendProps;
	string   Addr   = m_Properties.getStringValue ( "/fgls/addr" );
	uint32_t Port   = m_Properties.getIntValue    ( "/fgls/port" );
	NetAddr  fgls ( Addr, Port );

	SG_LOG ( SG_NETWORK, SG_DEBUG,
	  "requesting server #" << request_count << "..." );
	m_Peer.SetTarget (fgls);
	SendProps.push_back ( fgms::REQUEST_SERVER );
	WriteProps ( m_Peer, m_Properties, SendProps );
	m_Peer.Send ();
} // mpdummy::RequestServer ()

//////////////////////////////////////////////////////////////////////

void
mpdummy::Register
()
{
	static int request_count = 0;

	request_count++;
	if ( request_count > 3 )
	{
		SG_LOG ( SG_NETWORK, SG_WARN,
		  "No response to REGISTER, giving up..." );
		m_WantExit = true;
		return;
	}
	m_Peer.SetTarget ( m_FGMS.Addr );
	SG_LOG ( SG_NETWORK, SG_DEBUG,
	  "sending registration " << request_count << "..." );
	WriteProps ( m_Peer, m_Properties, m_OneTimeProperties );
	m_Peer.Send ();
} // mpdummy::Register ()

//////////////////////////////////////////////////////////////////////

void
mpdummy::SendMyData
()
{
	SG_LOG ( SG_NETWORK, SG_ALERT, "sending Data" );
	m_Peer.SetTarget ( m_FGMS.Addr );
	WriteProps ( m_Peer, m_Properties, m_ToSendProperties );
	WriteProps ( m_Peer, m_Properties, m_OnChangeProperties );
	m_Peer.Send ();
} // mpdummy::Register ()

//////////////////////////////////////////////////////////////////////

void
mpdummy::HandleCommand
(
	int Command,
	const NetAddr& Sender,
	t_ReceivePacket*  Packet
)
{
	switch (Command)
	{
	case fgms::AUTH_FAILED:
	case fgms::GENERAL_ERROR:
		SG_LOG ( SG_NETWORK, SG_ALERT, m_Properties.getStringValue (
		  "/multiplayer/system/errormessage") );
		if ( m_State != RUNNING )
			m_WantExit = true;
		break;
	case fgms::USE_SERVER:
		m_FGMS.Addr.Assign (
			m_Properties.getStringValue (
			  "/multiplayer/fgms/address" ),
			m_Properties.getIntValue    (
			  "/multiplayer/fgms/port" )
		);
		SG_LOG ( SG_NETWORK, SG_WARN, "using FGMS " << m_FGMS.Addr );
		SG_LOG ( SG_NETWORK, SG_INFO, "switching state to REGISTER" );
		m_State = REGISTER;
		break;
	case fgms::REGISTER_OK:
		SG_LOG ( SG_NETWORK, SG_DEBUG, "My ID is "
		  << m_Peer.GetSenderID () << ", registration succeeded.");
		if ( m_State < WAIT_AUTH )
		{
			SG_LOG ( SG_NETWORK, SG_INFO,
			  "switching state to WAIT_AUTH" );
			m_State = WAIT_AUTH;
		}
		break;
	case fgms::REGISTER_AGAIN:
		m_State = REGISTER;
		break;
	case fgms::PING:
		if ( Sender == m_FGMS.Addr )
		{
			m_FGMS.LastSeen = time ( 0 );
			m_FGMS.NumPingsRcvd++;
			SG_LOG ( SG_NETWORK, SG_INFO, "got PING from FGMS" );
		}
		else
		{
			SG_LOG ( SG_NETWORK, SG_ALERT, "got PING from unknown "
			  << Sender );
		}
		break;
	case fgms::PONG:
		if ( Sender == m_FGMS.Addr )
		{
			m_FGMS.LastSeen = time ( 0 );
			m_FGMS.NumPongs++;
			SG_LOG ( SG_NETWORK, SG_INFO, "got PONG from FGMS" );
		}
		else
		{
			SG_LOG ( SG_NETWORK, SG_ALERT, "got PONG from unknown "
			  << Sender );
		}
		break;
	case fgms::AUTHENTICATED:
		m_UpdateFrequency = m_Properties.getIntValue (
		  "/multiplayer/system/update_frequency" );
		cout << "update freq: " << m_UpdateFrequency << endl;
		SG_LOG ( SG_NETWORK, SG_INFO, "switching state to RUNNING" );
		m_State = RUNNING;
		break;
	case fgms::SET_CLIENT_ID:
		// nothing to do
		break;
	default:
		SG_LOG ( SG_NETWORK, SG_ALERT,
		  "unhandled command: " << Command );
	}
} // mpdummy::HandleCommand ()

//////////////////////////////////////////////////////////////////////

void
mpdummy::HandleRequest
()
{
	NetAddr	Sender;
	t_ReceivePacket*  Packet;

	m_Peer.Receive ();
	while (m_Peer.HasPackets())
	{
		Sender = m_Peer.GetSender ();
		try
		{
			Packet = m_Peer.NextPacket ();
		}
		catch (sg_exception & ex)
		{
			SG_LOG ( SG_NETWORK, SG_ALERT,
			  ex.getFormattedMessage () );
			continue;
		}
		if (Packet == 0)
		{
			SG_LOG ( SG_NETWORK, SG_ALERT, "no packet !" );
			continue;
		}
		fgms::t_PropertyList RcvdProps;
		ParsePacket (Packet, m_Peer, RcvdProps, m_Properties );
		while ( ! RcvdProps.empty () )
		{
			int Command = RcvdProps.front ();
			RcvdProps.pop_front ();
			if ( Command < fgms::INTERNAL_COMMAND )
			{
				HandleCommand (Command, Sender, Packet);
			}
		}
	}
} // mpdummy::HandleRequest ()

//////////////////////////////////////////////////////////////////////

void
mpdummy::WantExit
()
{
	m_WantExit = true;
} // mpdummy::WantExit ()

//////////////////////////////////////////////////////////////////////

void
mpdummy::Loop
()
{
	int Client;
	int AuthCount = 0;

	while ( m_WantExit == false )
	{
		if (m_State == REQUEST_SERVER)
		{
			RequestServer ();
		}
		else if (m_State == REGISTER)
		{
			Register ();
		}
		else if (m_State == WAIT_AUTH)
		{	// FIXME: what do we do if we receive no
			// AUTHENTICATED from fgms?
			AuthCount++;
			if ( AuthCount == 2 )
			{
			cout << "Authcount == 2!" << endl;
				AuthCount = 0;
				m_State = REGISTER;
			}
		}
		else if (m_State == RUNNING)
		{
			SendMyData ();
		}
		else
		{
			SG_LOG ( SG_NETWORK, SG_ALERT,
			  "mpdummy::Loop() - state?" );
		}
		Client = m_Peer.WaitForClients (m_UpdateFrequency);
		if (Client == UDP_Peer::ne_TIMEOUT)
		{
			continue;
		}
		if (! Client)
		{
			SG_LOG ( SG_NETWORK, SG_ALERT,
			  "mpdummy::Loop() - Bytes <= 0!" );
			continue;
		}
		if (Client > 0)
		{	// something on the wire (clients)
			HandleRequest ();
		}
	}
} // mpdummy::Loop ()

//////////////////////////////////////////////////////////////////////

mpdummy Server;

void
SigHandler
(
	int s
)
{
	SG_LOG ( SG_NETWORK, SG_ALERT, "caught signal " << s );
	SG_LOG ( SG_NETWORK, SG_ALERT, "exiting... " );
	Server.WantExit ();
	signal ( s, SigHandler );
} // SigCHLDHandler ()

int
main
(
	int argc,
	char* argv[]
)
{
	signal ( SIGINT, SigHandler );
	if (! Server.ParseParams ( argc, argv ) )
	{
		return ( fgms::FAILED );
	}
	if (! Server.Init () )
		return (fgms::FAILED);
	Server.Loop ();
	return (fgms::SUCCESS);
}

